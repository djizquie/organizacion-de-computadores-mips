		.data
		.align 2
autor:	.asciiz "\nautor: Daniel Izquierdo A."
archdisp: 	.asciiz "\ningrese el nombre de un archivo al cual hacerle las operaciones: \n-assange.txt\n-commandant.txt\n"
jumtable: 	.word procesoprincipal, case1, case2, case3, case4
r_op:	.asciiz "\nEl archivo tiene: "
mensaje_sinEspacio:	.asciiz "\narchivo guardado como whitoutspaces "
mensaje_op4:	.asciiz "\ninvertir palabras en este archivo: "
mensaje_invertido:	.asciiz "\narchivo guardado como reverse"
		.text

		.data

menu_principal:		.asciiz "\n\t1) contar palabras en el archivo\n\t2) Contar palabras con numero de caracter definido por el usuario\n\t3) Quitar espacios en blanco del archivo\n\t4) Invertir el archivo\n\n\t\tEscoja una opcion: "
		.text
		
		.data
no_encontrado:		.ascii  "no existe el archivo o esta danado"
		.space 23
arch_sin_espacio:	.ascii "withoutspaces.txt"
		.space 2000
arch_invertido:	.ascii "reverse.txt"
		.space 2000
a:		.ascii ""
cont: 		.ascii ""
buffer: 	.space 5000
		.text
		
		.data
sizearch:	.space 64
nChar:	.ascii "se buscaran palabras con este numero de caracteres: "
#numero:		.space 4
		.text

procesoprincipal:

	li 	$v0 4
	la 	$a0 autor		
	syscall
	li 	$v0 4
	la 	$a0 archdisp	
	syscall
	li	$v0 5
	##prueba edit
	##sll	$s0 $s0 1
	
	##li	$v0 4			#imprimir string
	##la 	$a0 mensaje_op1
	##syscall
	li 	$v0 8			#leer string
	la	$a0 sizearch	
	la 	$a1 64
	move 	$t6 $a0			
	move  	$s1 $a0			#nombre de archivo en in $s1
	syscall
	
	li $s0,0        # indice=0
	
	noEnter: #code from http://stackoverflow.com/questions/7969565/mips-how-to-store-user-input-string
		lb 	$a3,sizearch($s0)    	# carga el caracter en el indice
		addi 	$s0,$s0,1      			
   		bnez 	$a3,noEnter     			# sigue en el lazo hasta que la cadena se termina
   		beq 	$a1,$s0,continuar   		# Do not remove \n when string = maxlength
  		subiu 	$s0,$s0,2     			# If above not true, Backtrack index to '\n'
  		sb 	$0, sizearch($s0)  

 # open file code from : http://stackoverflow.com/questions/16027871/mips-file-opening
	li	$v0, 13		# Open File Syscall
	la	$a0, ($t6)	# Load File Name
	li	$a1, 0		# Read-only Flag
	li	$a2, 0		# (ignored)
	syscall
	move	$s6, $v0	# Save File Descriptor
	blt	$v0, 0, err	# Goto Error
	##
	
	syscall
	li 	$v0 4
	la 	$a0 menu_principal		
	syscall
	li	$v0 5
	syscall
	blez 	$v0 procesoprincipal
	li	$t3 4
	bgt	$v0 $t3 procesoprincipal
	la	$a1 jumtable
	sll	$t0 $v0 2
	add	$t1 $a1 $t0
	lw	$t2 0($t1)
	jr	$t2

case1:	
	
 
# lee los datos
	li	$v0, 14		# Read File Syscall
	move	$a0, $s6	# Load File Descriptor
	la	$a1, buffer	# Load Buffer Address
	li	$a2, 5000	# Buffer Size
	syscall
 
# Print Data
	#li	$v0, 4		# Print String Syscall
	li 	$t0, 1       	#Iniciamos los registros
	la	$t1, cont	# Load Contents String
	#syscall
	while:
    		lb 	$t2 0($t1)   		#Cargamos el primer caracter
    		la 	$t5 ' '
    		bne 	$t2 $t5 continuar  	#si t2 es igual espacio " " salta a continuar
		addi 	$t0 $t0 1    		#Incrementamos el n� de caracters
    		continuar:
    			blez $t2 finwhile  	#Si el caracter es igual a nulo salimos
	    		addi $t1 $t1 1    	#Apuntamos al siguiente caracter de la cadena
	    	j while     			#Repetimos el proceso

	finwhile: 
		li	$v0 4			#Codigo para imprimir un string
		la 	$a0 r_op	#Imprime en pantalla el string nombreArchivo
		syscall
	    	li 	$v0, 1       		#Mostrar por pantalla entero
	    	la 	$a0, ($t0)
	    	syscall

# Close File
close:
	li	$v0, 16		# Close File Syscall
	move	$a0, $s6	# Load File Descriptor
	syscall
	j	done		# Goto End
 
# Error
err:
	li	$v0, 4		# Print String Syscall
	la	$a0, no_encontrado	# Load Error String
	syscall
 

done:
	li	$v0, 10		# Exit Syscall
	syscall  
	b	salir
######################################################
case2:	
	sll	$s0 $s0 3
	

	
	li 	$v0 4   		#Codigo para imprimir un string
	la  	$a0 nChar 
	syscall           
	li 	$v0  5 			#Codigo para leer un integer  
	syscall
	move	$t7 $v0
	

# Read Data
	li	$v0, 14		# Read File Syscall
	move	$a0, $s6	# Load File Descriptor
	la	$a1, buffer	# Load Buffer Address
	li	$a2, 5000	# Buffer Size
	syscall
	
# Print Dat
	li 	$t0, 1       	#Iniciamos los registros
	la	$t1, cont	# Load Contents String
	li 	$t3, 0
	while2:
    		lb $t2, 0($t1)   			#Cargamos el primer caracter (cada elemento de una cadena tiene una longitud de 1Byte)
    		la $t5 ' '
    		bne $t2 $t5 continuar2
    			bgt $t7 $t3 continuarContar
			addi $t0, $t0,1    		#Incrementamos el n� de caracters
			li $t3, 0
			addi $t1, $t1, 1
    		continuar2:
    			blez $t2, finwhile2  		#Si el caracter es igual a nulo salimos
	    		addi $t1, $t1, 1    		#Apuntamos al siguiente caracter de la cadena
	    		addi $t3, $t3, 1 		#cuenta los caracteres de las palabras
	    		j while2     			#Repetimos el proceso
	    	continuarContar:
	    		blez $t2, finwhile2
	    		addi $t1, $t1, 1
	    		li $t3, 0 			#cuenta los caracteres de las palabras
	    	
	    	j while2     #Repetimos el proceso
	finwhile2: 
		li	$v0 4			#Codigo para imprimir un string
		la 	$a0 r_op		#Imprime en pantalla el string nombreArchivo
		syscall
	    	li 	$v0 1       		#Mostrar por pantalla entero
	    	la 	$a0 ($t0)
	    	syscall

# Close File
close2:
	li	$v0, 16		# Close File Syscall
	move	$a0, $s6	# Load File Descriptor
	syscall
	j	done		# Goto End
 
# Error
err2:
	li	$v0, 4		# Print String Syscall
	la	$a0, no_encontrado	# Load Error String
	syscall
	
	li	$v0, 4		#Print String Syscall
	la	$a0, ($s1)	#Load name of file·
	syscall
 
# Done
done2:
	li	$v0, 10		# Exit Syscall
	syscall
   
	b	salir
	
	
case3:
 
# lee los datos
	li	$v0, 14		# Read File Syscall
	move	$a0, $s6	# Load File Descriptor
	la	$a1, buffer	# Load Buffer Address
	li	$a2, 5000	# Buffer Size
	syscall
	
# load data
	li $t0, 1 				#Iniciamos los registros
	la $t1, cont				#cargar contenidos String
	la $t3 , a  				#cargamos  'a' a el registro t3
	la $t6 ' ' 


	while3:
		lb 	$t2, 0($t1)
		la 	$t5  ' ' 		#espacios
		beq 	$t2 $t5 continuar3
		sb 	$t2, 0($t3) 		#almacenamos el byte
		addi 	$t3, $t3, 1
		addi 	$t0, $t0, 1 		#incrmentamos el n� de caracteres
		sb 	$t6, 0($t1) 
		continuar3:
			blez $t2, finwhile3 	#si el caracter es igual a nulo salimos "por el espacio"
			addi $t1, $t1, 1 	#apuntamos  al sigueinte caracte
		j while3 
		
	finwhile3: 
		li	$v0 4			#Codigo para imprimir un string
		la 	$a0 mensaje_sinEspacio		#Imprime en pantalla el string nombreArchivo
		syscall 
	    	#li 	$v0 1      		#Mostrar por pantalla entero
	    	#la 	$a0 ($t0)
	    	#syscall
	#close the file
	li $v0, 16  			#systema llama a cerrar el archivo
	move $a0, $s6 			#archivo cierra
	
	#abrir  (para escribir) el archivo 
	li $v0, 13 			#llama funcion abrir archivo
	la $a0, arch_sin_espacio 		#salida del archivo SS
	li $a1, 1 			#abre y escribe en el archivo
	li $a2, 0 
	syscall
	move $s6, $v0

	#Write to file just opened
  	li   $v0, 15       		# system call for write to file
 	move $a0, $s6      		# file descriptor 
  	la   $a1, a   			# address of buffer from which to write
  	li   $a2, 5000       		# hardcoded buffer length
  	syscall           		# write to file           # open a file (file descriptor returned in $v0)
  	j done3
  	
 #Error
 err3:
 	li $v0, 4
 	la $a0, no_encontrado
	syscall
	
done3:
	li $v0, 10
	syscall
	sll	$s0 $s0 2
	b	salir
	

case4:	#invertir palabras
	
 
# lee los datos
	li	$v0, 14		# Read File Syscall
	move	$a0, $s6	# Load File Descriptor
	la	$a1, buffer	# Load Buffer Address
	li	$a2, 5000	# Buffer Size
	syscall
	
# load data
	li $t0, 0 			#Iniciamos los registros
	la $t3, cont 			#cargar contenidos String
	la $t1  a  			#cargamos  'a' a el registro t3
	la $t6 ' ' 
#nvertir string

	while4:
		lb 	$t2 0($t1)
		blez 	$t2 finwhile4 		#si el caracter es igual a nulo salimos "por el espacio"
		addi 	$t1 $t1 1
		addi 	$t0 $t0 1 		#apuntamos  al sigueinte caracte
		j while4
		
	finwhile4: 
	    	#li 	$v0 1       		#Mostrar por pantalla entero
	    	#move 	$a0, $t0
	    	#syscall
	    	
	subi 	$t0 $t0 1
	
	loop:	
		blt 	$t0 0 salir4
		la 	$t1 a
		add 	$t1 $t1 $t0
		lb 	$t2 0($t1)
		sb 	$t2 0($t3)
		addi 	$t3 $t3, 1
		
		#li 	$v0 11
		#move 	$a0 $t2
		#syscall
		
		subi 	$t0 $t0 1
		
		#addi $t1, $t1, 1
		j loop
salir4:
		li	$v0 4			#Codigo para imprimir un string
		la 	$a0 mensaje_invertido		#Imprime en pantalla el string nombreArchivo
		syscall
	
#close the file
	li 	$v0, 16  		#systema llama a cerrar el archivo
	move 	$a0, $s6 		#archivo cierra
	
#abrir  (para escribir) el archivo 
	li 	$v0, 13 		#llama funcion abrir archivo
	la 	$a0, arch_invertido 		#salida del archivo SS
	li 	$a1, 1 			#abre y escribe en el archivo
	li 	$a2, 0 
	syscall
	move 	$s6, $v0

#Write to file just opened
  	li   	$v0, 15       		# system call for write to file
 	move 	$a0, $s6      		# file descriptor 
  	la   	$a1, cont   		# address of buffer from which to write
  	li   	$a2, 5000       	# hardcoded buffer length
  	syscall            		# write to file           # open a file (file descriptor returned in $v0)
  	j 	done4
  	
 #Error
 err4:
 	li 	$v0, 4
 	la 	$a0, no_encontrado
	syscall
	
done4:
	li 	$v0, 10
	syscall
	b	salir

salir:	#FUNCION PARA SALIR DEL PROGRAMA
	li	$v0 10
	move	$a0 $a0
	syscall
